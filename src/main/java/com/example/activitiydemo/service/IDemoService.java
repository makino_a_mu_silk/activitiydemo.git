package com.example.activitiydemo.service;

public interface IDemoService {
	Object startTask(String taskName);

	Object getTask(String assignee);

	Object getTaskHistory(String proInsId);

	Object completeTask(String taskId, String assignee);

	Object returnAppointNode(String taskId, String assignee);

	Object returnAppointNode1(String taskId, String assignee);

	Object addTask(String taskId, String assignee);
}
