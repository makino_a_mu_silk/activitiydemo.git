package com.example.activitiydemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.activitiydemo.entity.ActGeBytearray;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ws
 * @since 2023-02-15
 */
public interface ActGeBytearrayMapper extends BaseMapper<ActGeBytearray> {

	List<ActGeBytearray> getActGeBytearrayList(String processDefinitionId);
}
