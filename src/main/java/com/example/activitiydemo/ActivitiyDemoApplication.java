package com.example.activitiydemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.activitiydemo.dao")
public class ActivitiyDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivitiyDemoApplication.class, args);
	}

}
