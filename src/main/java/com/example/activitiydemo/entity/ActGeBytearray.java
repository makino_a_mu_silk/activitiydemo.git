package com.example.activitiydemo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Blob;

/**
 * <p>
 * 
 * </p>
 *
 * @author ws
 * @since 2023-02-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
//@ApiModel(value="ActGeBytearray对象", description="")
public class ActGeBytearray implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID_")
    private String id;

    @TableField("REV_")
    private Integer rev;

    @TableField("NAME_")
    private String name;

    @TableField("DEPLOYMENT_ID_")
    private String deploymentId;

    @TableField("BYTES_")
    private byte[] bytes;

    @TableField("GENERATED_")
    private Integer generated;


}
