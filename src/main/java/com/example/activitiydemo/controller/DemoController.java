package com.example.activitiydemo.controller;

import com.example.activitiydemo.service.IDemoService;
//import io.swagger.annotations.ApiOperation;
import com.example.activitiydemo.utils.FlowUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

@RestController
public class DemoController {

	@Resource
	IDemoService service;

	@GetMapping("/startTask")
//	@ApiOperation("启动任务:目前只有test;helloword;")
	public Object startTask(String taskName) {
		return service.startTask(taskName);
	}

	@GetMapping("/getTask")
//	@ApiOperation("查询人员待执行任务")
	public Object getTask(String assignee) {
		return service.getTask(assignee);
	}

	@GetMapping("/getTaskHistory")
//	@ApiOperation("查询任务历史")
	public Object getTaskHistory(String proInsId) {
		return service.getTaskHistory(proInsId);
	}

	@GetMapping("/completeTask")
//	@ApiOperation("完成任务")
	public Object completeTask(String taskId, String assignee) {
		return service.completeTask(taskId, assignee);
	}

	@GetMapping("/rollBackToAppointNode")
//	@ApiOperation("多人会签节点的退回指定节点")
	public Object returnAppointNode(String taskId, String assignee) {
		return service.returnAppointNode(taskId, assignee);
	}

	@GetMapping("/rollBackToAppointNode1")
//	@ApiOperation("退回到指定多人会签节点")
	public Object returnAppointNode1(String taskId, String assignee) {
		return service.returnAppointNode1(taskId, assignee);
	}

	@GetMapping("/addTask")
//	@ApiOperation("新增节点")
	public Object addTask(String taskId, String assignee) {
		return service.addTask(taskId,assignee);
	}

	@Resource
	FlowUtils flowUtils;


	/**
	 * 查看实例流程图，根据流程实例ID获取流程图
	 */
	@RequestMapping(value="traceprocess",method= RequestMethod.GET)
	public void traceprocess(HttpServletResponse response, @RequestParam("proInsId")String proInsId,@RequestParam("fileName") String fileName) throws Exception{
		InputStream in = flowUtils.getResourceDiagramInputStream(proInsId);
		File file = new File("C:\\Users\\wangshuai9776\\"+ fileName +".svg");
		OutputStream output = new FileOutputStream(file);
//		ServletOutputStream output = response.getOutputStream();
		IOUtils.copy(in, output);
	}
}
