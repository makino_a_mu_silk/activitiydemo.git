package com.example.activitiydemo;

import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

@SpringBootTest
class ActivitiyDemoApplicationTests {

//	public static void main(String[] args) {
//		//获取流程引擎
//		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//
//		//存储服务
//		RepositoryService repositoryService = processEngine.getRepositoryService();
//		//运行服务
//		RuntimeService runtimeService = processEngine.getRuntimeService();
//		//任务服务
//		TaskService taskService = processEngine.getTaskService();
//
//		//发布流程
//		repositoryService.createDeployment().addClasspathResource("first.bpmn").deploy();
//
//		//创建一个请假流程，获取流程实例。此处的流程id为first.bpmn的process的id
//		ProcessInstance pi = runtimeService.startProcessInstanceByKey("myProcess_1");
//
//		//员工完成请假的任务
//		Task task = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
//		System.out.println("当前流程节点：" + task.getName());
//		taskService.complete(task.getId());
//
//		//经理完成审批任务
//		task = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
//		System.out.println("当前流程节点：" + task.getName());
//		taskService.complete(task.getId());
//
//		//流程执行完了，再去获取任务会为null
//		task = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
//		System.out.println("流程执行结束了：" + task);
//
//		//关闭引擎
//		processEngine.close();
//		//退出
//		System.exit(0);
//
//	}

	/**
	 * 生成 activiti的数据库表
	 */
	@Test
	public void testCreateDbTable() {
		//默认创建方式
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		//通用的创建方式，指定配置文件名和Bean名称
//        ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml", "processEngineConfiguration");
//        ProcessEngine processEngine1 = processEngineConfiguration.buildProcessEngine();
		System.out.println(processEngine);

	}
	@Test
	public void testCreateDbTable1(){
//        需要使用avtiviti提供的工具类 ProcessEngines ,使用方法getDefaultProcessEngine
//        getDefaultProcessEngine会默认从resources下读取名字为actviti.cfg.xml的文件
//        创建processEngine时，就会创建mysql的表

//        默认方式
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//        repositoryService.createDeployment();

//        使用自定义方式
//        配置文件的名字可以自定义,bean的名字也可以自定义
		ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration.
				createProcessEngineConfigurationFromResource("activiti.cfg.xml",
						"processEngineConfiguration");

//        获取流程引擎对象
		ProcessEngine processEngine = processEngineConfiguration.buildProcessEngine();

		RuntimeService runtimeService = processEngine.getRuntimeService();
		System.out.println(processEngine);
	}

	/**
	 * 部署流程定义  文件上传方式
	 */
	@Test
	public void testDeployment(){
//        1、创建ProcessEngine
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        2、得到RepositoryService实例
		RepositoryService repositoryService = processEngine.getRepositoryService();
//        3、使用RepositoryService进行部署
		Deployment deployment = repositoryService.createDeployment()
				.addClasspathResource("bpmn/test.bpmn") // 添加bpmn资源
				//png资源命名是有规范的。Leave.myLeave.png|jpg|gif|svg  或者Leave.png|jpg|gif|svg
				.addClasspathResource("bpmn/test.png")  // 添加png资源
				.name("请假申请流程")
				.deploy();
//        4、输出部署信息
		System.out.println("流程部署id：" + deployment.getId());
		System.out.println("流程部署名称：" + deployment.getName());
	}

	/**
	 * zip压缩文件上传方式
	 */
	@Test
	public void deployProcessByZip() {
		// 定义zip输入流
		InputStream inputStream = this
				.getClass()
				.getClassLoader()
				.getResourceAsStream(
						"bpmn/Leave.zip");
		ZipInputStream zipInputStream = new ZipInputStream(inputStream);
		// 获取repositoryService
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		// 流程部署
		Deployment deployment = repositoryService.createDeployment()
				.addZipInputStream(zipInputStream)
				.deploy();
		System.out.println("流程部署id：" + deployment.getId());
		System.out.println("流程部署名称：" + deployment.getName());
	}

	/**
	 * 启动流程实例
	 */
	@Test
	public void testStartProcess(){
//        1、创建ProcessEngine
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        2、获取RunTimeService
		RuntimeService runtimeService = processEngine.getRuntimeService();
		ProcessDefinition processDefinition = processEngine.getRepositoryService()
				.createProcessDefinitionQuery()
				.singleResult();
//        3、根据流程定义Id启动流程
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("test");
//        输出内容
		System.out.println("流程定义id：" + processInstance.getProcessDefinitionId());
		System.out.println("流程实例id：" + processInstance.getId());
		System.out.println("当前活动Id：" + processInstance.getActivityId());
	}

	/**
	 * 查询当前个人待执行的任务
	 */
	@Test
	public void testFindPersonalTaskList() {
//        任务负责人
//		String assignee = "张三";
		String assignee = "李四1";
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        创建TaskService
		TaskService taskService = processEngine.getTaskService();
//        根据流程key 和 任务负责人 查询任务
		List<Task> list = taskService.createTaskQuery()
				.processDefinitionKey("test") //流程Key
				.taskAssignee(assignee)//只查询该任务负责人的任务
				.list();

		for (Task task : list) {
			System.out.println("----------------------------");
			System.out.println("流程实例id：" + task.getProcessInstanceId());
			System.out.println("任务id：" + task.getId());
			System.out.println("任务负责人：" + task.getAssignee());
			System.out.println("任务名称：" + task.getName());

		}
	}

	/**
	 * 查询流程定义
	 */
	@Test
	public void queryProcessDefinition(){
		//        获取引擎
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        repositoryService
		RepositoryService repositoryService = processEngine.getRepositoryService();
//        得到ProcessDefinitionQuery 对象
		ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
//          查询出当前所有的流程定义
//          条件：processDefinitionKey =evection
//          orderByProcessDefinitionVersion 按照版本排序
//        desc倒叙
//        list 返回集合
		List<ProcessDefinition> definitionList = processDefinitionQuery.processDefinitionKey("helloworld")
				.orderByProcessDefinitionVersion()
				.desc()
				.list();
//      输出流程定义信息
		for (ProcessDefinition processDefinition : definitionList) {
			System.out.println("----------------------------");
			System.out.println("流程定义 id="+processDefinition.getId());
			System.out.println("流程定义 name="+processDefinition.getName());
			System.out.println("流程定义 key="+processDefinition.getKey());
			System.out.println("流程定义 Version="+processDefinition.getVersion());
			System.out.println("流程部署ID ="+processDefinition.getDeploymentId());
		}
	}

	@Test
	public void deleteDeployment() {
		// 流程部署id
		String deploymentId = "1";

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		// 通过流程引擎获取repositoryService
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		//删除流程定义，如果该流程定义已有流程实例启动则删除时出错
		repositoryService.deleteDeployment(deploymentId);
		//设置true 级联删除流程定义，即使该流程有流程实例启动也可以删除，设置为false非级别删除方式，如果流程
//        repositoryService.deleteDeployment(deploymentId, true);
	}

	/**
	 * 查询流程实例
	 */
	@Test
	public void queryProcessInstance() {
		// 流程定义key
		String processDefinitionKey = "helloworld";
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		// 获取RunTimeService
		RuntimeService runtimeService = processEngine.getRuntimeService();
		List<ProcessInstance> list = runtimeService
				.createProcessInstanceQuery()
				.processDefinitionKey(processDefinitionKey)//
				.list();

		for (ProcessInstance processInstance : list) {
			System.out.println("----------------------------");
			System.out.println("流程实例id："
					+ processInstance.getProcessInstanceId());
			System.out.println("所属流程定义id："
					+ processInstance.getProcessDefinitionId());
			System.out.println("是否执行完成：" + processInstance.isEnded());
			System.out.println("是否暂停：" + processInstance.isSuspended());
			System.out.println("当前活动标识：" + processInstance.getActivityId());
			System.out.println("业务关键字："+processInstance.getBusinessKey());
		}
	}

//	@Test //流程资源下载
//	public void  queryBpmnFile() throws IOException {
////        1、得到引擎
//		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
////        2、获取repositoryService
//		RepositoryService repositoryService = processEngine.getRepositoryService();
////        3、得到查询器：ProcessDefinitionQuery，设置查询条件,得到想要的流程定义
//		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
//				.processDefinitionKey("myLeave")
//				.singleResult();
////        4、通过流程定义信息，得到部署ID
//		String deploymentId = processDefinition.getDeploymentId();
////        5、通过repositoryService的方法，实现读取图片信息和bpmn信息
////        png图片的流
//		InputStream pngInput = repositoryService.getResourceAsStream(deploymentId, processDefinition.getDiagramResourceName());
////        bpmn文件的流
//		InputStream bpmnInput = repositoryService.getResourceAsStream(deploymentId, processDefinition.getResourceName());
////        6、构造OutputStream流
//		File file_png = new File("d:/myLeave.png");
//		File file_bpmn = new File("d:/myLeave.bpmn");
//		FileOutputStream bpmnOut = new FileOutputStream(file_bpmn);
//		FileOutputStream pngOut = new FileOutputStream(file_png);
////        7、输入流，输出流的转换
//		IOUtils.copy(pngInput,pngOut);
//		IOUtils.copy(bpmnInput,bpmnOut);
////        8、关闭流
//		pngOut.close();
//		bpmnOut.close();
//		pngInput.close();
//		bpmnInput.close();
//	}


	/**
	 * 查看历史信息
	 */
	@Test
	public void findHistoryInfo(){
//      获取引擎
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取HistoryService
		HistoryService historyService = processEngine.getHistoryService();
//        获取 actinst表的查询对象
		HistoricActivityInstanceQuery instanceQuery = historyService.createHistoricActivityInstanceQuery();
//        查询 actinst表，条件：根据 InstanceId 查询，查询一个流程的所有历史信息
		instanceQuery.processInstanceId("5001");
//        查询 actinst表，条件：根据 DefinitionId 查询，查询一种流程的所有历史信息
//        instanceQuery.processDefinitionId("myLeave:1:22504");
//        增加排序操作,orderByHistoricActivityInstanceStartTime 根据开始时间排序 asc 升序
		instanceQuery.orderByHistoricActivityInstanceStartTime().asc();
//        查询所有内容
		List<HistoricActivityInstance> activityInstanceList = instanceQuery.list();
//        输出
		for (HistoricActivityInstance hi : activityInstanceList) {
			if (hi.getActivityName() != null) {
				System.out.println(hi.getActivityId());
				System.out.println(hi.getActivityName());
				System.out.println(hi.getProcessDefinitionId());
				System.out.println(hi.getProcessInstanceId());
				System.out.println("<==========================>");
			}
		}
	}

	/**
	 * 流程部署
	 */
//	@Test
//	public void testDeployment(){
////        1、创建ProcessEngine
//		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
////        2、获取RepositoryServcie
//		RepositoryService repositoryService = processEngine.getRepositoryService();
////        3、使用service进行流程的部署，定义一个流程的名字，把bpmn和png部署到数据中
//		Deployment deploy = repositoryService.createDeployment()
//				.name("出差申请流程-variables")
//				.addClasspathResource("bpmn/evection-global.bpmn")
//				.deploy();
////        4、输出部署信息
//		System.out.println("流程部署id="+deploy.getId());
//		System.out.println("流程部署名字="+deploy.getName());
//	}

	/**
	 * 启动流程 的时候设置流程变量
	 * 设置流程变量num
	 * 设置任务负责人
	 */
//	@Test
//	public void testStartProcess(){
////        获取流程引擎
//		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
////        获取RunTimeService
//		RuntimeService runtimeService = processEngine.getRuntimeService();
////        流程定义的Key
//		String key = "myEvection2";
////        流程变量的map
//		Map<String,Object> variables = new HashMap<>();
//////        设置流程变量
////		Evection evection = new Evection();
//////        设置出差日期
////		evection.setNum(2d);
//////        把流程变量的pojo放入map
////		variables.put("evection",evection);
////        设定任务的负责人
//		variables.put("assignee0","李四");
//		variables.put("assignee1","王经理");
//		variables.put("assignee2","杨总经理");
//		variables.put("assignee3","张财务");
////        启动流程
//		runtimeService.startProcessInstanceByKey(key,variables);
//	}

	@Test
	public void queryTask(){
//        流程定义的Key
		String key = "test";
//        任务负责人
//        String assingee = "李四";
		//        获取流程引擎
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取taskservice
		TaskService taskService = processEngine.getTaskService();
//        查询任务
		final List<Task> tasks = taskService.createTaskQuery()
				.processDefinitionKey(key)
//                .taskAssignee(assingee)
				.list();
		for(Task task:tasks){
			//     根据任务id来   完成任务
			System.out.println(task.getId());
			System.out.println(task.getName());
			System.out.println(task.getAssignee());
		}

	}
	/**
	 * 完成个人任务
	 */
	@Test
	public void completTask(){
//        流程定义的Key
		String key = "helloworld";
//        任务负责人
//		String assingee = "张三";
//		String assingee = "李四";
		String assingee = "王五";

//        String assingee = "张财务";
		//        获取流程引擎
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取taskservice
		TaskService taskService = processEngine.getTaskService();
//        查询任务
		Task task = taskService.createTaskQuery()
				.processDefinitionKey(key)
				.taskAssignee(assingee)
				.singleResult();
		if(task != null){
			//     根据任务id来   完成任务
			taskService.complete(task.getId());
		}

	}

	@Test
	public void delTask() {
		String id = "2501";

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

		final RepositoryService repositoryService = processEngine.getRepositoryService();
		repositoryService.deleteDeployment(id);

	}
}
